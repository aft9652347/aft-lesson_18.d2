# Diplom_2_UI

Вторая часть дипломного проекта.
Выполнена работа по тестированию UI https://opensource-demo.orangehrmlive.com/

Проект выполнен на Maven.
Используемые библиотеки: TestNG, Lombok, Faker, Allure, Jackson Databind, Selenide.
Используемые плагины: Apache Maven Dependency Plugin, Allure plugin.

**Функционал покрытый тестами в рамках данной работы:**
- Логирование;
- Переход по пунктам главного меню/переход на главные страницы разделов приложения;
- Тест страницы Admin.

