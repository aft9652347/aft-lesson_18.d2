package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class AdminPage {

    SelenideElement headerTitleAdmin =$x("//h6[text()='Admin']");
    SelenideElement headerTitleUserManagement =$x("//h6[text()='User Management']");

    SelenideElement usernameField = $x("//*[@id='app']//div[2]/input");
    SelenideElement UsernameFilter = $x("//*[@id='app']//div[2]/input");
    SelenideElement userRoleListBox = $x("//*[@id='app']//form//div/div[2]/div//div/div/div[2]/i");
    //Список ролей для выбора в выпадающем списке
    public SelenideElement userRoleBoxListBoxSelectorESS = $x("//span[contains(text(), 'ESS')]");
    public SelenideElement userRoleBoxListBoxSelectorAdmin = $x("//span[contains(text(), 'Admin')]");

    //Проверка наличия заголовка Admin
    @Step("Проверить наличия заголовка Admin")
    public AdminPage showHeaderTitleAdmin(){
        headerTitleAdmin
                .shouldBe(visible);
        return this;
    }

    //Проверка наличия заголовка User Management
    @Step("Проверить наличия заголовка User Management")
    public AdminPage showHeaderTitleUserManagement(){
        headerTitleUserManagement
                .shouldBe(visible);
        return this;
    }

    //Проверка отображения заголовка User Management

    //Нажать на кнопку для открытия спика ролей и выбрать роль Admin/ESS
    @Step("Нажать на кнопку для открытия спиcка ролей и выбрать роль: ")
    public AdminPage clickSelectRoleListDown(SelenideElement userRole){
        userRoleListBox.click();
        userRole.click();
        return this;
    }





    //gjkmpjdfntkm ecc
    //пользователь админ



    //не равен Admin admin есть, а ess нет
    //Найти все элементы таблицы проверить на равенство

    //Проверить по пользвоваетелям и их ролям


}
