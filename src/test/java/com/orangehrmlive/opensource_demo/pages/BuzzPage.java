package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class BuzzPage {
    //Заголовок страницы "BuzzPage"
    SelenideElement headerTitleBuzzPage =$x("//h6[text()='Buzz']");

    //Проверка наличия заголовка BuzzPage
    @Step("Проверить наличия заголовка Buzz")
    public BuzzPage showHeaderTitleBuzzPage(){
        headerTitleBuzzPage
                .shouldBe(visible);
        return this;
    }
}
