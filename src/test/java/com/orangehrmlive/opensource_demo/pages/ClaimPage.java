package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ClaimPage {
    //Заголовок страницы "ClaimPage"
    SelenideElement headerTitleClaimPage =$x("//h6[text()='Claim']");

    //Проверка наличия заголовка ClaimPage
    @Step("Проверить наличия заголовка Directory")
    public ClaimPage showHeaderTitleClaimPage(){
        headerTitleClaimPage
                .shouldBe(visible);
        return this;
    }
}
