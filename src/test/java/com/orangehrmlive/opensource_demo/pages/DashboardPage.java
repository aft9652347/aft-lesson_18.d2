package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class DashboardPage {

    //Заголовок страницы "DashboardPage"
    SelenideElement headerTitleDashboardPage =$x("//h6[text()='Dashboard']");

    //Проверка наличия заголовка Dashboard
    @Step("Проверить наличия заголовка Dashboard")
    public DashboardPage showHeaderTitleDashboard(){
        headerTitleDashboardPage
                .shouldBe(visible);
        return this;
    }
}
