package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class DirectoryPage {
    //Заголовок страницы "DirectoryPage"
    SelenideElement headerTitleDirectoryPage =$x("//h6[text()='Directory']");

    //Проверка наличия заголовка DirectoryPage
    @Step("Проверить наличия заголовка Directory")
    public DirectoryPage showHeaderTitleDirectory(){
        headerTitleDirectoryPage
                .shouldBe(visible);
        return this;
    }
}
