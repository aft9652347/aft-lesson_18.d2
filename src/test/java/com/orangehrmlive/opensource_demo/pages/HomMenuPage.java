package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class HomMenuPage {
    //Строка поиска
    SelenideElement searchString = $x("//input[@class='oxd-input oxd-input--active']");
    //Перечисление пунктов меню
    SelenideElement itemMenuAdmin = $x("//span[text() = 'Admin']");
    SelenideElement itemMenuPIM = $x("//span[text() = 'PIM']");
    SelenideElement itemMenuLeave = $x("//span[text() = 'Leave']");
    SelenideElement itemMenuTime = $x("//span[text() = 'Time']");
    SelenideElement itemMenuRecruitment = $x("//span[text() = 'Recruitment']");
    SelenideElement itemMenuMyInfo = $x("//span[text() = 'My Info']");
    SelenideElement itemMenuPerformance = $x("//span[text() = 'Performance']");
    SelenideElement itemMenuDashboard = $x("//span[text() = 'Dashboard']");
    SelenideElement itemMenuDirectory = $x("//span[text() = 'Directory']");

    SelenideElement itemMenuMaintenance = $x("//span[text() = 'Maintenance']");
    SelenideElement administratorAccessPassword = $x("//input[@name='password']");
    SelenideElement administratorAccessButtonConfirm= $x("//button[text() =' Confirm ']");

    SelenideElement itemMenuClaim = $x("//span[text() = 'Claim']");
    SelenideElement itemMenuBuzz = $x("//span[text() = 'Buzz']");

    @Step("Проверить отображение строки поиска на странице")
    public HomMenuPage showSearchString(){
       searchString
                .shouldBe(visible);
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Admin\"")
    public HomMenuPage clickItemMenuAdmin(){
        itemMenuAdmin.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"PIM\"")
    public HomMenuPage clickItemMenuPIM(){
        itemMenuPIM.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Leave\"")
    public HomMenuPage clickItemMenuLeave(){
        itemMenuLeave.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Time\"")
    public HomMenuPage clickItemMenuTime(){
        itemMenuTime.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Recruitment\"")
    public HomMenuPage clickItemMenuRecruitment(){
        itemMenuRecruitment.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"My Info\"")
    public HomMenuPage clickItemMenuMyInfo(){
        itemMenuMyInfo.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Performance\"")
    public HomMenuPage clickItemMenuPerformance(){
        itemMenuPerformance.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Dashboard\"")
    public HomMenuPage clickItemMenuDashboard(){
        itemMenuDashboard.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Directory\"")
    public HomMenuPage clickItemMenuDirectory(){
        itemMenuDirectory.click();
        return this;
    }

    @Step("Нажать и перейти в пункт меню \"Maintenance\"")
    public HomMenuPage enterItemMenuMaintenance(String password){
        itemMenuMaintenance.click();
        administratorAccessPassword.sendKeys(password);
        administratorAccessButtonConfirm.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Claim\"")
    public HomMenuPage clickItemMenuClaim(){
        itemMenuClaim.click();
        return this;
    }

    @Step("Нажать(перейти) пункт меню \"Buzz\"")
    public HomMenuPage clickItemMenuBuzz(){
        itemMenuBuzz.click();
        return this;
    }


}
