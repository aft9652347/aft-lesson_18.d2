package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LeavePage {
    //Заголовок страницы "LeavePage"
    SelenideElement headerTitleLeavePage =$x("//h6[text()='Leave']");

    //Проверка наличия заголовка Leave
    @Step("Проверить наличия заголовка Leave")
    public LeavePage showHeaderTitleLeave(){
        headerTitleLeavePage
                .shouldBe(visible);
        return this;
    }

}
