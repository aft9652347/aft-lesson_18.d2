package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    SelenideElement fieldLogin = $x("//input[@name='username']");
    SelenideElement fieldPassword = $x("//input[@name='password']");
    SelenideElement buttonLigon = $x("//button[@class='oxd-button oxd-button--medium oxd-button--main orangehrm-login-button']");
    public SelenideElement notificationRequiredLogin =$x("//*[@id=\"app\"]//form/div[1]//span");
    public SelenideElement notificationRequiredPassword = $x("//*[@id=\"app\"]//form/div[2]//span");
    SelenideElement alertInvalidCredentials = $x("//p[text()='Invalid credentials']");

    @Step("Открыть страницу логирования")
    public void open(){
        Selenide.open("https://opensource-demo.orangehrmlive.com");
    }
    @Step("Ввести логин: {login}")
    public LoginPage setFieldLogin(String login){
        fieldLogin.sendKeys(login);
        return this;
    }
    @Step("Ввести пароль: {password}")
    public LoginPage setFieldPassword(String password){
        fieldPassword.sendKeys(password);
        return this;
    }
    @Step("Нажать кнопку \"Login\"")
    public void clickButtonLogin(){
        buttonLigon.click();
    }

    @Step("Проверить отображение уведомления об обязательности заполнения поля \"Required}\"")
    public LoginPage shownRequiredLoginPage(SelenideElement showRequired){
        showRequired
                .shouldBe(visible)
                .shouldHave(text("Required"));
        return this;
    }

    @Step("Проверить отображение алерта с сообщением \"Invalid credentials\"")
    public LoginPage showAlertInvalidCredentials(){
        alertInvalidCredentials
                .shouldBe(visible)
                .shouldHave(text("Invalid credentials"));
        return this;
    }

    @Step("Выполнить авторизацию логин:{login}, пароль:{password}")
    public void authorization(String login, String password){
        setFieldLogin(login);
        setFieldPassword(password);
        clickButtonLogin();





    }


}
