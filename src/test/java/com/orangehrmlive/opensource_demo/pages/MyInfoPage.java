package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class MyInfoPage {
    //Заголовок страницы "My Info"
    SelenideElement headerTitleMyInfoPage =$x("//h6[text()='My Info']");

    //Проверка наличия заголовка My Info
    @Step("Проверить наличия заголовка My Info")
    public MyInfoPage showHeaderTitleMyInfo(){
        headerTitleMyInfoPage
                .shouldBe(visible);
        return this;
    }
}
