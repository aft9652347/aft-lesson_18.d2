package com.orangehrmlive.opensource_demo.pages;

public interface PageObjectSupplier {

    default LoginPage loginPage() {return new LoginPage();}
    default HomMenuPage homeMenuPage(){return new HomMenuPage();}
    default AdminPage adminPage(){return new AdminPage();}
    default PimPage pimPage(){return new PimPage();}
    default LeavePage leavePage(){return new LeavePage();}
    default TimePage timePage(){return new TimePage();}
    default RecruitmentPage recruitmentPage(){return new RecruitmentPage();}
    default MyInfoPage myInfoPage(){return new MyInfoPage();}
    default PerformancePage performancePage(){return new PerformancePage();}
    default DashboardPage dashboardPage(){return new DashboardPage();}
    default DirectoryPage directoryPage(){return new DirectoryPage();}
    default MaintenancePage maintenancePage(){return new MaintenancePage();}
    default ClaimPage claimPage(){return new ClaimPage();}
    default BuzzPage buzzPage(){return new BuzzPage();}

}
