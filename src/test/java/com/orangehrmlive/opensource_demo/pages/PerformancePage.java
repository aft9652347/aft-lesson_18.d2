package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class PerformancePage {

    //Заголовок страницы "PerformancePage"
    SelenideElement headerTitlePerformancePage =$x("//h6[text()='Performance']");

    //Проверка наличия заголовка PerformancePage
    @Step("Проверить наличия заголовка Leave")
    public PerformancePage showHeaderTitlePerformance(){
        headerTitlePerformancePage
                .shouldBe(visible);
        return this;
    }

}
