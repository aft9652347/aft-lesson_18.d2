package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class PimPage {
    //Заголовок страницы "PIM"
    SelenideElement headerTitlePimPage =$x("//h6[text()='PIM']");

    //Проверка наличия заголовка PIM
    @Step("Проверить наличия заголовка PIM")
    public PimPage showHeaderTitlePIM(){
        headerTitlePimPage
                .shouldBe(visible);
        return this;
    }

}
