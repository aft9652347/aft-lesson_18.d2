package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class RecruitmentPage {

    //Заголовок страницы "RecruitmentPage"
    SelenideElement headerTitleRecruitmentPage =$x("//h6[text()='Recruitment']");

    //Проверка наличия заголовка Recruitment
    @Step("Проверить наличия заголовка Recruitment")
    public RecruitmentPage showHeaderTitleRecruitment(){
        headerTitleRecruitmentPage
                .shouldBe(visible);
        return this;
    }

}
