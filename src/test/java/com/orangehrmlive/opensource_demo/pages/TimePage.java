package com.orangehrmlive.opensource_demo.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class TimePage {

    //Заголовок страницы "TimePage"
    SelenideElement headerTitleTimePage =$x("//h6[text()='Time']");

    //Проверка наличия заголовка Time
    @Step("Проверить наличия заголовка Time")
    public TimePage showHeaderTitleTime(){
        headerTitleTimePage
                .shouldBe(visible);
        return this;
    }

}
