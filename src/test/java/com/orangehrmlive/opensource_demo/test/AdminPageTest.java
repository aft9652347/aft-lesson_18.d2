package com.orangehrmlive.opensource_demo.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.orangehrmlive.opensource_demo.pages.AdminPage;
import com.orangehrmlive.opensource_demo.pages.PageObjectSupplier;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AdminPageTest implements PageObjectSupplier {

    @DataProvider(name = "dataProviderRole")
    public Object[] dataProviredRole() {
        return new Object[]{
                adminPage().userRoleBoxListBoxSelectorESS,
                adminPage().userRoleBoxListBoxSelectorAdmin

        };
    }


    @BeforeMethod
    public void setUp() {
        //loginPage().open();
        Configuration.remote = "http://51.250.123.179:4444/wb/hub"; //18 урок 01:27
        loginPage().authorization("Admin","admin123");
    }

    @AfterMethod
    public void closeBrowser() {
        Selenide.closeWindow();
    }

    @Test(/*dataProvider = "dataProviderRole"*/)
    public void FilterRoleUserListTestESS(){
        adminPage().clickSelectRoleListDown(adminPage().userRoleBoxListBoxSelectorESS);
        //.shouldBe(visible);
        /**сайт прилёг с 500 не успел доделать**/
    }

    @Test()
    public void FilterRoleUserListTestAdmin(){
        adminPage().clickSelectRoleListDown(adminPage().userRoleBoxListBoxSelectorAdmin);
        //.shouldBe(visible);
        /**сайт прилёг с 500 не успел доделать**/
    }











}
