package com.orangehrmlive.opensource_demo.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.orangehrmlive.opensource_demo.pages.PageObjectSupplier;
import io.qameta.allure.Description;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

public class HomeMenuTest implements PageObjectSupplier {
    //Проверить переходы по всем пунктам меню, проверять отображение наименований хедоре

    @BeforeMethod
    public void setUp() {
        //loginPage().open();

//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        Configuration.remote = "http://51.250.123.179:4444/wb/hub";
//        Configuration.browserCapabilities = capabilities;
        loginPage().authorization("Admin","admin123");
    }

    @AfterMethod
    public void closeBrowser() {
        Selenide.closeWindow();
    }

    //Проверить работу переходов по пунктам меню
    @Test()
    @Description("Проверка работы меню и возможности перехода на страницу Admin")
    public void enterItemsAdminPage(){
        homeMenuPage().clickItemMenuAdmin();
        adminPage()
                .showHeaderTitleAdmin()
                .showHeaderTitleUserManagement();
    }

    @Test
    @Description("Проверка перехода на страницу  \"PIM\"")
    public void enterItemsPimPage(){
        homeMenuPage().clickItemMenuPIM();
        pimPage().showHeaderTitlePIM();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Leave\"")
    public void enterItemsLeavePage(){
        homeMenuPage().clickItemMenuLeave();
        leavePage().showHeaderTitleLeave();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Time\"")
    public void enterItemsTimePage(){
        homeMenuPage().clickItemMenuTime();
        timePage().showHeaderTitleTime();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Recruitment\"")
    public void enterItemsRecruitmentPage(){
        homeMenuPage().clickItemMenuRecruitment();
        recruitmentPage().showHeaderTitleRecruitment();
    }

    @Test
    @Description("Проверка перехода на страницу  \"My Info\"")
    public void enterItemsMyInfoPage(){
        homeMenuPage().clickItemMenuMyInfo();
        myInfoPage().showHeaderTitleMyInfo();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Performance\"")
    public void enterItemsPerformancePage(){
        homeMenuPage().clickItemMenuPerformance();
        performancePage().showHeaderTitlePerformance();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Dashboard\"")
    public void enterItemsDashboardPage(){
        homeMenuPage().clickItemMenuDashboard();
        dashboardPage().showHeaderTitleDashboard();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Directory\"")
    public void enterItemsDirectoryPage(){
        homeMenuPage().clickItemMenuDirectory();
        directoryPage().showHeaderTitleDirectory();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Maintenance\"")
    public void enterItemsMaintenancePage(){
        homeMenuPage().enterItemMenuMaintenance("admin123");
        maintenancePage().showHeaderTitleMaintenance();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Claim\"")
    public void enterItemsClaimPage(){
        homeMenuPage().clickItemMenuClaim();
        claimPage().showHeaderTitleClaimPage();
    }

    @Test
    @Description("Проверка перехода на страницу  \"Claim\"")
    public void enterItemsBuzzPage(){
        homeMenuPage().clickItemMenuBuzz();
        buzzPage().showHeaderTitleBuzzPage();
    }







}
