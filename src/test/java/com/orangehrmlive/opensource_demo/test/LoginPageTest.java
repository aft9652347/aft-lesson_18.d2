package com.orangehrmlive.opensource_demo.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.github.javafaker.Faker;
import com.orangehrmlive.opensource_demo.pages.PageObjectSupplier;
import io.qameta.allure.Description;
import org.testng.annotations.*;

public class LoginPageTest implements PageObjectSupplier {

//    LoginPage loginPage;
    Faker faker=new Faker();

//    @DataProvider(name = "negativeDateLogin")
//    public Object[][] negativeDateLogin() {
//        return new Object[][]{
//                {"AS", "212"},
//                {faker.name().lastName(),faker.internet().password()},
//                {"", ""},
//                {"","15"},
//                {"","admin123"}
//        };
//    }

    @BeforeMethod
    public void setUp() {
        //loginPage().open();
        Configuration.remote = "http://51.250.123.179:4444/wb/hub";
    }
    @AfterMethod
    public void closeBrowser() {
        Selenide.closeWindow();
    }

    //Позитивный тест логирования
    @Test()
    @Description("Проверка авторизации верные login|password")
    public void positiveLoginTest() {
        loginPage()
                .setFieldLogin("Admin")
                .setFieldPassword("admin123")
                .clickButtonLogin();
        homeMenuPage()
                .showSearchString();

    }

    //Проверка отображения Required при не заполненных обязательных полях
    @Test
    @Description("Проверка отображения уведомления Required (авторизации/уведомления). Пустое значение в поле login")
    public void positiveRequiredLoginTest() {
        loginPage()
                .setFieldLogin("")
                .setFieldPassword(faker.internet().password())
                .clickButtonLogin();
        loginPage().shownRequiredLoginPage(loginPage().notificationRequiredLogin);

    }

    @Test
    @Description("Проверка отображения уведомления Required авторизации/уведомления. Пустые значения login|password")
    public void positiveRequiredPasswordTest(){
        loginPage()
                .setFieldLogin("")
                .setFieldPassword("")
                .clickButtonLogin();
        loginPage().shownRequiredLoginPage(loginPage().notificationRequiredLogin);
        loginPage().shownRequiredLoginPage(loginPage().notificationRequiredPassword);
    }

    @Test
    @Description("Проверка авторизации/алерта \"InvalidCredentials\". Неверная login|password от другой учётки")
    public void positiveAlertTest(){
        loginPage()
                .setFieldLogin(faker.name().lastName())
                .setFieldPassword("admin123")
                .clickButtonLogin();
        loginPage().showAlertInvalidCredentials();


    }


}



